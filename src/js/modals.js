// MOdal
!function(e){"function"!=typeof e.matches&&(e.matches=e.msMatchesSelector||e.mozMatchesSelector||e.webkitMatchesSelector||function(e){for(var t=this,o=(t.document||t.ownerDocument).querySelectorAll(e),n=0;o[n]&&o[n]!==t;)++n;return Boolean(o[n])}),"function"!=typeof e.closest&&(e.closest=function(e){for(var t=this;t&&1===t.nodeType;){if(t.matches(e))return t;t=t.parentNode}return null})}(window.Element.prototype);


document.addEventListener('DOMContentLoaded', function() {
   var modalButtons = document.querySelectorAll('.js-open-modal'),
       overlay      = document.querySelector('.modal-wrapper'),
       closeButtons = document.querySelectorAll('.js-modal-close');

   modalButtons.forEach(function(item){
      item.addEventListener('click', function(e) {
         e.preventDefault();
         var modalId = this.getAttribute('data-modal'),
             modalElem = document.querySelector('.modal[data-modal="' + modalId + '"]');

         modalElem.classList.add('--show');
         overlay.classList.add('--show');
         document.querySelector('body').classList.add('body-overflow');
      }); 

   }); 


   closeButtons.forEach(function(item){

      item.addEventListener('click', function(e) {
        e.preventDefault();
         var parentModal = this.closest('.modal');

         parentModal.classList.remove('--show');
         overlay.classList.remove('--show');
         document.querySelector('body').classList.remove('body-overflow');
      });

   }); 


    document.body.addEventListener('keyup', function (e) {
        var key = e.keyCode;

        if (key == 27) {

            document.querySelector('.modal.--show').classList.remove('--show');
            document.querySelector('.overlay').classList.remove('--show');
            document.querySelector('body').classList.remove('body-overflow');
        };
    }, false);


    overlay.addEventListener('click', function() {
        document.querySelector('.modal.--show').classList.remove('--show');
        this.classList.remove('--show');
        document.querySelector('body').classList.remove('body-overflow');
    });




}); 


$(document).on('click', '.open-dialog', function() {
   $('.mechan-createblock').toggleClass('--hide');
   $('.mechan-addblock').toggleClass('--hide');
});

$(document).on('click', '.close-dialog', function() {
   $('.mechan-createblock').toggleClass('--hide');
   $('.mechan-addblock').toggleClass('--hide');
});
## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:8081
$ npm run dev

# build for production
$ npm run build

